/*************************************************************
 * CIS 361 W17 Project 1 - Automatic Caesar Cipher Breaker
 *
 * This program takes a file containing letter frequencies
 * for letters in the alphabet and a file that contians an 
 * encrypted using a Caesar cipher. The program finds the key
 * used to encrypt the message and then decrypts it. The
 * passed file will then contain the decrypted version of the
 * message.
 *
 * @author Kyle Hekhuis
 ************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

static const int ALPHA_LEN = 26; //Length of alphabet

void readFreq(float[], FILE *);
void calcFreq(float[], FILE *);
char rotate(char, int);
int findKey(float[], float[]);
void decrypt(int, FILE *);

/****************************************************
 * Takes the letter frequency and a file containing
 * an encrypted message, then decrypts the message.
 ***************************************************/
int main(int argc, char* argv[]) {
	//Check if correct number of command line args were passed
	if (argc != 2) {
		printf ("Usage: decrypt LetFreq.txt datafile.txt\n");
		exit(1);
	}
	
	FILE *letFreq = fopen("LetFreq.txt", "r");
	FILE *datafile = fopen(argv[1], "r+");
	//Check if files opened properly
	if (!letFreq || !datafile) {
		printf("File(s) failed to open\n");
		exit(1);
	}

	float given[ALPHA_LEN];
	float found[ALPHA_LEN] = {0};
	readFreq(given, letFreq);
	calcFreq(found, datafile);
	int key = findKey(given, found);
	decrypt(key, datafile);

	fclose(letFreq);
	fclose(datafile);

	return 0;
}

/*****************************************************
 * Reads the passed text file of letter frequencies
 * then puts each letters frequency into the passed
 * array into the corresponding spot. array[0] = 'A'
 * and so on.
 *
 * @param given[] array of floats to hold letter
 * 				  frequencies
 * @param *letFreq pointer to file containing letter
 * 				  frequenices
 *****************************************************/
void readFreq(float given[], FILE *letFreq) {
	char* line = malloc(11 * sizeof(char)); //Holds full line as in 'A 0.08167'
	
	for (int i = 0; i < ALPHA_LEN; i++) {
		fgets(line, 11, letFreq); //Get full line of letter freq
		given[i] = atof( (line + 2) ); //Advance by 2 to only get freq
	}

	free(line);
}

/*****************************************************
 * Reads the passed file containing the encrypted
 * message. Calculates the frequencies of the letters
 * in the file and then puts them into the passed
 * array into the correspoding spot. array[0] = 'A'
 * and so on.
 *
 * @param found[] array of floats to hold letter
 * 				  frequencies
 * @param *datafile pointer to file containing the
 * 				    encrypted message.
 ****************************************************/
void calcFreq(float found[], FILE *datafile) {
	int numOfChars = 0; //Amount of alphabetic chars in file
	char ch;
	
	//Read characters and store number of occurances of each
	//letter into array
	while ( (ch = getc(datafile) ) != EOF) {
		if (isalpha(ch)) {
			numOfChars++;
			found[(int)(toupper(ch) - 'A')]++;
		}
	}
	
	//printf("In datafile, the letter frequeinces are:\n");
	
	//Calculate letter frequenices by dividing each
	//amount of letters by total letters
	for (int i = 0; i < ALPHA_LEN; i++) {
		if  (found[i] > 0) {
			found[i] = found[i] / numOfChars;
		}
		//printf("%c has freq of %.5f\n", (char)(i + 65), found[i]);
	}
}

/*******************************************************
 * Rotates the passed character down the alphabet the
 * number of positions given in passed integer and then
 * returns it. Ex. Ch = 'A' and num = 2. Returned char
 * is 'C'.
 *
 * @param ch character to rotate from
 * @param num number of positions to rotate
 * @return rotated character
 ******************************************************/
char rotate(char ch, int num) {
	if (isupper(ch)) {
		ch = (ch - 'A' + num) % ALPHA_LEN + 'A';
	} else {
		ch = (ch - 'a' + num) % ALPHA_LEN + 'a';
	}
	return ch;
}

/************************************************************
 * Finds the key used in the Caesar cipher to orginally
 * encrypt the message. Does so by comparing the
 * frequencies in the given frequencies of letters and
 * the found frequenices of letters in the encrypted file.
 * Performs a sum of the square of the differences between
 * given and found frequenices. Rotates the found frequencies
 * one at a time to find every possible sum. Gets the lowest
 * sum and returns the key that corresponds to that sum.
 *
 * @param given[] array of given letter frequencies
 * @param found[] array of found letter frequencies
 * @return key of cipher used to decrypt this message
 ***********************************************************/
int findKey(float given[], float found[]) {
	float differences[ALPHA_LEN]; //Array to hold differences
	int key;
	float sum1 = 0; //Lowest sum
	float sum2 = 0; //Current iteration sum
	
	//i is equal to key value
	for (int i = 0; i <= ALPHA_LEN; i++) {
		//j is position in given array
		for (int j = 0; j < ALPHA_LEN; j++) {
			//Shift is position in found array that corresponds
			//to the rotated position. Do mod to wrap it back
			//around.
			int shift = (j + i) % ALPHA_LEN;
			differences[j] = pow((given[j] - found[shift]), 2);
			sum2 += differences[j];
		}
		//Check if current iteration sum less than lowest sum.
		//Set lowest to current iteration sum if on first
		//iteration.
		if ((sum2 < sum1) || i == 0) {
			sum1 = sum2;
			key = i; //Save key for lowest sum
		}
		//Reset iteration sum to 0 for next iteration
		sum2 = 0;
	}
	
	printf("The key is: %d\n", key);

	return key;
}

/**********************************************
 * Decrypts the passed encrypted file with the
 * passed cipher key.
 *********************************************/
void decrypt(int key, FILE *datafile) {
	rewind(datafile); //Rewind to set file position back to 0
	char ch;

	while ( (ch = getc(datafile)) != EOF) {
		//Only want to rotate letters
		if (isalpha(ch)) {
			//Rotate letter to correct one
			ch = rotate(ch, (ALPHA_LEN - key));
			//Set file position back 1 so char is put
			//in correct position
			fseek(datafile, -1, SEEK_CUR);
			fputc(ch, datafile);
		}
	}

	printf("File decrypted\n");
}
