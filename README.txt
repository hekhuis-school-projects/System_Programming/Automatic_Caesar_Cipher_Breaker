Automatic Caesar Cipher Breaker
Language: C
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
         Prof. Yonglei Tao (http://cis.gvsu.edu/~tao/)
Class: CIS 361- System Programming
Semester / Year: Winter 2017

This program decrypts simple Caesar ciphers that were encrypted using 'cipher'.
It does so using frequencies of letters in the English language found in the
'LetFreq.txt' file. For full specifications see 'Project 1 - Automatic Caesar Cipher Breaker.pdf'.

Compile with either gcc or clang on Linux.

Usage:
To encrypt:
./cipher <cipher option (1 to encrypt)> <key> <inputData> <outputData>
To decrypt:
./decrypt <outputData>