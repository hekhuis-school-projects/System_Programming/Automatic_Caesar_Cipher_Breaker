all: cipher decrypt

cipher: cipher.o
	clang cipher.o -o cipher
cipher.o: cipher.c

decrypt: decrypt.c
	clang decrypt.c -o decrypt -lm -g

test: test1 test2
test1:
	./cipher 1 5 TestDataShort.txt dataShort.txt
	./decrypt dataShort.txt
	diff -s TestDataShort.txt dataShort.txt
test2:
	./cipher 2 5 TestDataLong.txt dataLong.txt
	./decrypt dataLong.txt
	diff -s TestDataLong.txt dataLong.txt
